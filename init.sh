#!/bin/bash

PS3='Please enter your choice: '
options=("Install Alpine Packages" "Install Debian Packages" "Install RedHat Packages" "Install ZSH" "Install VIM Settings" "SSH-Key: home" "SSH-Key: swt" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Install Alpine Packages")
            sudo apk add curl git shadow vim zsh
            ;;
        "Install Debian Packages")
            sudo apt install curl git vim zsh
            ;;
        "Install RedHat Packages")
            sudo dnf install tmux zsh git
            ;;
        "Install ZSH")
            echo "Install ZSH"
            sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

            wget https://gitlab.com/jhonnens/init/-/raw/master/files/jan.zsh-theme -O ~/.oh-my-zsh/themes/jan.zsh-theme

            wget https://gitlab.com/jhonnens/init/-/raw/master/files/zshrc -O /tmp/zshrc_tmp
            sed -i 's/^ZSH_THEME.*/ZSH_THEME="jan"/' ~/.zshrc
            sed -i 's/^plugins=.*/plugins=(git colorize cp)/' ~/.zshrc
            sed -i '/###USERMOD###/,$d' ~/.zshrc
            cat /tmp/zshrc_tmp >> ~/.zshrc
            rm /tmp/zshrc_tmp
            #chsh -s $(which zsh)
            ;;
        "Install VIM Settings")
            wget https://gitlab.com/jhonnens/init/-/raw/master/files/vimrc -O ~/.vimrc
            ;;
        "SSH-Key: home")
            echo "Install home SSH-Keys"
            mkdir -p ~/.ssh/
            chmod 700 ~/.ssh/
            touch ~/.ssh/authorized_keys
            chmod 600 ~/.ssh/authorized_keys

            sed -i '/jho_gaming/d' .ssh/authorized_keys
            sed -i '/jho_home/d' .ssh/authorized_keys
            sed -i '/jho_macos-2023/d' .ssh/authorized_keys

            echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDefqwSzMzFrwzFsexLkcl36VI911jDyFM56azRDiQop jho_gaming" >> ~/.ssh/authorized_keys
            #echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK2k+R/LwPXmZWGuf4DyDcZoCmn8ZDld+uy4kP/+Adpz jho_home" >> ~/.ssh/authorized_keys
            echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAID6wj20xCY+5uoMyXJFa9IafTMe95WSmqnX7o7wfiBGJ jho_macos-2023" >> ~/.ssh/authorized_keys
            ;;
        "SSH-Key: swt")
            echo "Install tuenet SSH-Keys"
            mkdir -p ~/.ssh/
            chmod 700 ~/.ssh/
            touch ~/.ssh/authorized_keys
            chmod 600 ~/.ssh/authorized_keys

            sed -i '/jho@tuenet/d' .ssh/authorized_keys

            echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAVIBIiCAYKfbpXxa+wy7/Ndmvb5oV9AC/cJGYPsoScT jho@tuenet" >> ~/.ssh/authorized_keys
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
